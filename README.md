# test_cb_dockers

Pull the directory and run 'docker-compose up -d' to Start all the services at once and to stop run 'docker-compose down'

rabbitmq-node-1: @port:15672
rabbitmq-node-2: @port:15673 (Clustered)

Nginx-serving-flask-app: @port:2048

#put the data related to rabbitmq into respective node directory in storage directory.